# Ico Website

To install

* php composer.phar install	
* setup firebase database and create private key - https://console.firebase.google.com/project/icoplatform/settings/serviceaccounts/adminsdk
* create config.php and firebase.json
* Point your virtual host document root to your new application's `public/` directory.
* Ensure `logs/` is web writeable.

To run the application in development, you can also run this command. 

	php composer.phar start

Run this command to run the test suite

	php composer.phar test

That's it! Now go build something cool.

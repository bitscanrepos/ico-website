<?php
// DIC configuration

$container = $app->getContainer();

// config
$container['config'] = new Jasny\Config((include __DIR__.'/../config.php'));

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// firebase
$container['firebase'] = function ($c) {
    $settings = $c->get('config')->credentials->firebase;
    $firebase = \Firebase::fromServiceAccount(__DIR__.'/../firebase.json');
    return $firebase->getDatabase();
};
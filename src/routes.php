<?php
// Routes

$app->get('/', function ($request, $response, $args) {
    // Sample log message
    // $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/firebase', function($request, $response, $args) {

    $newPost = $this->firebase
        ->getReference('blog/posts')
        ->push([
            'title' => 'Post title',
            'body' => 'This should probably be longer.'
        ]);

    echo $newPost->getKey(); // => -KVr5eu8gcTv7_AHb-3-
    echo '<br>',$newPost->getUri(); // => https://[domain].firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-

    exit();

});

<?php

return [
    'credentials' => [
        'firebase' => [
            'url' => '',
            'token' => '',
        ],
        'stripe' => [
            'secret' => '',
            'publishable' => ''
        ],
        'escrow' => [
            'key' => '',
        ],
        'waves' => [
            'key' => '',
        ],
        'printful' => [
            'key' => '',
        ]
    ]
];